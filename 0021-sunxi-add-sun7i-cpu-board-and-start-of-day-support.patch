From 785ba593d5fa67ed2997ce937cafd5286459beaf Mon Sep 17 00:00:00 2001
From: Ian Campbell <ijc@hellion.org.uk>
Date: Fri, 18 Apr 2014 19:05:45 +0100
Subject: [PATCH 21/36] sunxi: add sun7i cpu, board and start of day support
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This patch adds generic board, start of day and basic build system support for
the Allwinner A20 (sun7i) processor. This code will not been compiled until the
build is hooked up in a later patch. It has been split out to keep the patches
manageable.

Signed-off-by: Adam Sampson <ats@offog.org>
Signed-off-by: Aleksei Mamlin <mamlinav@gmail.com>
Signed-off-by: Alexandru Gagniuc <mr.nuke.me@gmail.com>
Signed-off-by: Chen-Yu Tsai <wens@csie.org>
Signed-off-by: Emilio López <emilio@elopez.com.ar>
Signed-off-by: Hans de Goede <hdegoede@redhat.com>
Signed-off-by: Henrik Nordstrom <henrik@henriknordstrom.net>
Signed-off-by: Jens Kuske <jenskuske@gmail.com>
Signed-off-by: Luc Verhaegen <libv@skynet.be>
Signed-off-by: Luke Leighton <lkcl@lkcl.net>
Signed-off-by: Oliver Schinagl <oliver@schinagl.nl>
Signed-off-by: Patrick Wood <patrickhwood@gmail.com>
Signed-off-by: Stefan Roese <sr@denx.de>
Signed-off-by: Wills Wang <wills.wang.open@gmail.com>
Signed-off-by: Ian Campbell <ijc@hellion.org.uk>
Cc: Tom Cubie <Mr.hipboi@gmail.com>
---
 arch/arm/cpu/armv7/Makefile                 |   2 +-
 arch/arm/cpu/armv7/sunxi/Makefile           |  11 +++
 arch/arm/cpu/armv7/sunxi/board.c            |  88 +++++++++++++++++
 arch/arm/cpu/armv7/sunxi/cpu_info.c         |  19 ++++
 arch/arm/cpu/armv7/sunxi/start.c            |   1 +
 arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds |  77 +++++++++++++++
 arch/arm/include/asm/arch-sunxi/cpu.h       | 122 ++++++++++++++++++++++++
 arch/arm/include/asm/arch-sunxi/spl.h       |  20 ++++
 board/sunxi/Makefile                        |  11 +++
 board/sunxi/board.c                         |  57 +++++++++++
 include/configs/sun7i.h                     |  24 +++++
 include/configs/sunxi-common.h              | 141 ++++++++++++++++++++++++++++
 12 files changed, 572 insertions(+), 1 deletion(-)
 create mode 100644 arch/arm/cpu/armv7/sunxi/board.c
 create mode 100644 arch/arm/cpu/armv7/sunxi/cpu_info.c
 create mode 100644 arch/arm/cpu/armv7/sunxi/start.c
 create mode 100644 arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds
 create mode 100644 arch/arm/include/asm/arch-sunxi/cpu.h
 create mode 100644 arch/arm/include/asm/arch-sunxi/spl.h
 create mode 100644 board/sunxi/Makefile
 create mode 100644 board/sunxi/board.c
 create mode 100644 include/configs/sun7i.h
 create mode 100644 include/configs/sunxi-common.h

diff --git a/arch/arm/cpu/armv7/Makefile b/arch/arm/cpu/armv7/Makefile
index 119ebb3..ddf00f3 100644
--- a/arch/arm/cpu/armv7/Makefile
+++ b/arch/arm/cpu/armv7/Makefile
@@ -12,7 +12,7 @@ obj-y	+= cache_v7.o
 obj-y	+= cpu.o
 obj-y	+= syslib.o
 
-ifneq ($(CONFIG_AM43XX)$(CONFIG_AM33XX)$(CONFIG_OMAP44XX)$(CONFIG_OMAP54XX)$(CONFIG_TEGRA)$(CONFIG_MX6)$(CONFIG_TI81XX)$(CONFIG_AT91FAMILY),)
+ifneq ($(CONFIG_AM43XX)$(CONFIG_AM33XX)$(CONFIG_OMAP44XX)$(CONFIG_OMAP54XX)$(CONFIG_TEGRA)$(CONFIG_MX6)$(CONFIG_TI81XX)$(CONFIG_AT91FAMILY)$(CONFIG_SUNXI),)
 ifneq ($(CONFIG_SKIP_LOWLEVEL_INIT),y)
 obj-y	+= lowlevel_init.o
 endif
diff --git a/arch/arm/cpu/armv7/sunxi/Makefile b/arch/arm/cpu/armv7/sunxi/Makefile
index d81d26c..a64bfa1 100644
--- a/arch/arm/cpu/armv7/sunxi/Makefile
+++ b/arch/arm/cpu/armv7/sunxi/Makefile
@@ -8,7 +8,18 @@
 # SPDX-License-Identifier:	GPL-2.0+
 #
 obj-y	+= timer.o
+obj-y	+= board.o
 obj-y	+= clock.o
 obj-y	+= pinmux.o
 obj-$(CONFIG_SUN7I)	+= clock_sun4i.o
+
+ifndef CONFIG_SPL_BUILD
+obj-y	+= cpu_info.o
+endif
+
+ifdef CONFIG_SPL_BUILD
 obj-$(CONFIG_SUN7I)	+= dram.o
+ifdef CONFIG_SPL_FEL
+obj-y	+= start.o
+endif
+endif
diff --git a/arch/arm/cpu/armv7/sunxi/board.c b/arch/arm/cpu/armv7/sunxi/board.c
new file mode 100644
index 0000000..b5c0cb7
--- /dev/null
+++ b/arch/arm/cpu/armv7/sunxi/board.c
@@ -0,0 +1,88 @@
+/*
+ * (C) Copyright 2012 Henrik Nordstrom <henrik@henriknordstrom.net>
+ *
+ * (C) Copyright 2007-2011
+ * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
+ * Tom Cubie <tangliang@allwinnertech.com>
+ *
+ * Some init for sunxi platform.
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#include <common.h>
+#include <serial.h>
+#ifdef CONFIG_SPL_BUILD
+#include <spl.h>
+#endif
+#include <asm/gpio.h>
+#include <asm/io.h>
+#include <asm/arch/clock.h>
+#include <asm/arch/gpio.h>
+#include <asm/arch/sys_proto.h>
+#include <asm/arch/timer.h>
+
+#ifdef CONFIG_SPL_BUILD
+/* Pointer to the global data structure for SPL */
+DECLARE_GLOBAL_DATA_PTR;
+
+/* The sunxi internal brom will try to loader external bootloader
+ * from mmc0, nand flash, mmc2.
+ * Unfortunately we can't check how SPL was loaded so assume
+ * it's always the first SD/MMC controller
+ */
+u32 spl_boot_device(void)
+{
+	return BOOT_DEVICE_MMC1;
+}
+
+/* No confirmation data available in SPL yet. Hardcode bootmode */
+u32 spl_boot_mode(void)
+{
+	return MMCSD_MODE_RAW;
+}
+#endif
+
+int gpio_init(void)
+{
+	sunxi_gpio_set_cfgpin(SUNXI_GPB(22), SUN4I_GPB22_UART0_TX);
+	sunxi_gpio_set_cfgpin(SUNXI_GPB(23), SUN4I_GPB23_UART0_RX);
+	sunxi_gpio_set_pull(SUNXI_GPB(23), 1);
+
+	return 0;
+}
+
+void reset_cpu(ulong addr)
+{
+}
+
+/* do some early init */
+void s_init(void)
+{
+#if !defined CONFIG_SPL_BUILD && (defined CONFIG_SUN7I || defined CONFIG_SUN6I)
+	/* Enable SMP mode for CPU0, by setting bit 6 of Auxiliary Ctl reg */
+	asm volatile(
+		"mrc p15, 0, r0, c1, c0, 1\n"
+		"orr r0, r0, #1 << 6\n"
+		"mcr p15, 0, r0, c1, c0, 1\n");
+#endif
+
+	clock_init();
+	timer_init();
+	gpio_init();
+
+#ifdef CONFIG_SPL_BUILD
+	gd = &gdata;
+	preloader_console_init();
+
+	sunxi_board_init();
+#endif
+}
+
+#ifndef CONFIG_SYS_DCACHE_OFF
+void enable_caches(void)
+{
+	/* Enable D-cache. I-cache is already enabled in start.S */
+	dcache_enable();
+}
+#endif
diff --git a/arch/arm/cpu/armv7/sunxi/cpu_info.c b/arch/arm/cpu/armv7/sunxi/cpu_info.c
new file mode 100644
index 0000000..b4c3d5c
--- /dev/null
+++ b/arch/arm/cpu/armv7/sunxi/cpu_info.c
@@ -0,0 +1,19 @@
+/*
+ * (C) Copyright 2007-2011
+ * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
+ * Tom Cubie <tangliang@allwinnertech.com>
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#include <common.h>
+#include <asm/io.h>
+#include <asm/arch/cpu.h>
+
+#ifdef CONFIG_DISPLAY_CPUINFO
+int print_cpuinfo(void)
+{
+	puts("CPU:   Allwinner A20 (SUN7I)\n");
+	return 0;
+}
+#endif
diff --git a/arch/arm/cpu/armv7/sunxi/start.c b/arch/arm/cpu/armv7/sunxi/start.c
new file mode 100644
index 0000000..6b392fa
--- /dev/null
+++ b/arch/arm/cpu/armv7/sunxi/start.c
@@ -0,0 +1 @@
+/* Intentionally empty. Only needed to get FEL SPL link line right */
diff --git a/arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds b/arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds
new file mode 100644
index 0000000..364e35c
--- /dev/null
+++ b/arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds
@@ -0,0 +1,77 @@
+/*
+ * (C) Copyright 2013
+ * Henrik Nordstrom <henrik@henriknordstrom.net>
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+OUTPUT_FORMAT("elf32-littlearm", "elf32-littlearm", "elf32-littlearm")
+OUTPUT_ARCH(arm)
+ENTRY(s_init)
+SECTIONS
+{
+	. = 0x00002000;
+
+	. = ALIGN(4);
+	.text :
+	{
+		*(.text.s_init)
+		*(.text*)
+	}
+
+	. = ALIGN(4);
+	.rodata : { *(SORT_BY_ALIGNMENT(SORT_BY_NAME(.rodata*))) }
+
+	. = ALIGN(4);
+	.data : {
+		*(.data*)
+	}
+
+	. = ALIGN(4);
+	. = .;
+
+	. = ALIGN(4);
+	.rel.dyn : {
+		__rel_dyn_start = .;
+		*(.rel*)
+		__rel_dyn_end = .;
+	}
+
+	.dynsym : {
+		__dynsym_start = .;
+		*(.dynsym)
+	}
+
+	. = ALIGN(4);
+	.note.gnu.build-id :
+	{
+		*(.note.gnu.build-id)
+	}
+	_end = .;
+
+	. = ALIGN(4096);
+	.mmutable : {
+		*(.mmutable)
+	}
+
+	.bss_start __rel_dyn_start (OVERLAY) : {
+		KEEP(*(.__bss_start));
+		__bss_base = .;
+	}
+
+	.bss __bss_base (OVERLAY) : {
+		*(.bss*)
+		. = ALIGN(4);
+		__bss_limit = .;
+	}
+
+	.bss_end __bss_limit (OVERLAY) : {
+		KEEP(*(.__bss_end));
+	}
+
+	/DISCARD/ : { *(.dynstr*) }
+	/DISCARD/ : { *(.dynamic*) }
+	/DISCARD/ : { *(.plt*) }
+	/DISCARD/ : { *(.interp*) }
+	/DISCARD/ : { *(.gnu*) }
+	/DISCARD/ : { *(.note*) }
+}
diff --git a/arch/arm/include/asm/arch-sunxi/cpu.h b/arch/arm/include/asm/arch-sunxi/cpu.h
new file mode 100644
index 0000000..7400559
--- /dev/null
+++ b/arch/arm/include/asm/arch-sunxi/cpu.h
@@ -0,0 +1,122 @@
+/*
+ * (C) Copyright 2007-2011
+ * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
+ * Tom Cubie <tangliang@allwinnertech.com>
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#ifndef _SUNXI_CPU_H
+#define _SUNXI_CPU_H
+
+#define SUNXI_SRAM_A1_BASE		0x00000000
+#define SUNXI_SRAM_A1_SIZE		(16 * 1024)	/* 16 kiB */
+
+#define SUNXI_SRAM_A2_BASE		0x00004000	/* 16 kiB */
+#define SUNXI_SRAM_A3_BASE		0x00008000	/* 13 kiB */
+#define SUNXI_SRAM_A4_BASE		0x0000b400	/* 3 kiB */
+#define SUNXI_SRAM_D_BASE		0x01c00000
+#define SUNXI_SRAM_B_BASE		0x01c00000	/* 64 kiB (secure) */
+
+#define SUNXI_SRAMC_BASE		0x01c00000
+#define SUNXI_DRAMC_BASE		0x01c01000
+#define SUNXI_DMA_BASE			0x01c02000
+#define SUNXI_NFC_BASE			0x01c03000
+#define SUNXI_TS_BASE			0x01c04000
+#define SUNXI_SPI0_BASE			0x01c05000
+#define SUNXI_SPI1_BASE			0x01c06000
+#define SUNXI_MS_BASE			0x01c07000
+#define SUNXI_TVD_BASE			0x01c08000
+#define SUNXI_CSI0_BASE			0x01c09000
+#define SUNXI_TVE0_BASE			0x01c0a000
+#define SUNXI_EMAC_BASE			0x01c0b000
+#define SUNXI_LCD0_BASE			0x01c0C000
+#define SUNXI_LCD1_BASE			0x01c0d000
+#define SUNXI_VE_BASE			0x01c0e000
+#define SUNXI_MMC0_BASE			0x01c0f000
+#define SUNXI_MMC1_BASE			0x01c10000
+#define SUNXI_MMC2_BASE			0x01c11000
+#define SUNXI_MMC3_BASE			0x01c12000
+#define SUNXI_USB0_BASE			0x01c13000
+#define SUNXI_USB1_BASE			0x01c14000
+#define SUNXI_SS_BASE			0x01c15000
+#define SUNXI_HDMI_BASE			0x01c16000
+#define SUNXI_SPI2_BASE			0x01c17000
+#define SUNXI_SATA_BASE			0x01c18000
+#define SUNXI_PATA_BASE			0x01c19000
+#define SUNXI_ACE_BASE			0x01c1a000
+#define SUNXI_TVE1_BASE			0x01c1b000
+#define SUNXI_USB2_BASE			0x01c1c000
+#define SUNXI_CSI1_BASE			0x01c1d000
+#define SUNXI_TZASC_BASE		0x01c1e000
+#define SUNXI_SPI3_BASE			0x01c1f000
+
+#define SUNXI_CCM_BASE			0x01c20000
+#define SUNXI_INTC_BASE			0x01c20400
+#define SUNXI_PIO_BASE			0x01c20800
+#define SUNXI_TIMER_BASE		0x01c20c00
+#define SUNXI_SPDIF_BASE		0x01c21000
+#define SUNXI_AC97_BASE			0x01c21400
+#define SUNXI_IR0_BASE			0x01c21800
+#define SUNXI_IR1_BASE			0x01c21c00
+
+#define SUNXI_IIS_BASE			0x01c22400
+#define SUNXI_LRADC_BASE		0x01c22800
+#define SUNXI_AD_DA_BASE		0x01c22c00
+#define SUNXI_KEYPAD_BASE		0x01c23000
+#define SUNXI_TZPC_BASE			0x01c23400
+#define SUNXI_SID_BASE			0x01c23800
+#define SUNXI_SJTAG_BASE		0x01c23c00
+
+#define SUNXI_TP_BASE			0x01c25000
+#define SUNXI_PMU_BASE			0x01c25400
+#define SUNXI_CPUCFG_BASE              0x01c25c00
+
+#define SUNXI_UART0_BASE		0x01c28000
+#define SUNXI_UART1_BASE		0x01c28400
+#define SUNXI_UART2_BASE		0x01c28800
+#define SUNXI_UART3_BASE		0x01c28c00
+#define SUNXI_UART4_BASE		0x01c29000
+#define SUNXI_UART5_BASE		0x01c29400
+#define SUNXI_UART6_BASE		0x01c29800
+#define SUNXI_UART7_BASE		0x01c29c00
+#define SUNXI_PS2_0_BASE		0x01c2a000
+#define SUNXI_PS2_1_BASE		0x01c2a400
+
+#define SUNXI_TWI0_BASE			0x01c2ac00
+#define SUNXI_TWI1_BASE			0x01c2b000
+#define SUNXI_TWI2_BASE			0x01c2b400
+
+#define SUNXI_CAN_BASE			0x01c2bc00
+
+#define SUNXI_SCR_BASE			0x01c2c400
+
+#define SUNXI_GPS_BASE			0x01c30000
+#define SUNXI_MALI400_BASE		0x01c40000
+#define SUNXI_GMAC_BASE			0x01c50000
+
+/* module sram */
+#define SUNXI_SRAM_C_BASE		0x01d00000
+
+#define SUNXI_DE_FE0_BASE		0x01e00000
+#define SUNXI_DE_FE1_BASE		0x01e20000
+#define SUNXI_DE_BE0_BASE		0x01e60000
+#define SUNXI_DE_BE1_BASE		0x01e40000
+#define SUNXI_MP_BASE			0x01e80000
+#define SUNXI_AVG_BASE			0x01ea0000
+
+/* CoreSight Debug Module */
+#define SUNXI_CSDM_BASE			0x3f500000
+
+#define SUNXI_DDRII_DDRIII_BASE		0x40000000	/* 2 GiB */
+
+#define SUNXI_BROM_BASE			0xffff0000	/* 32 kiB */
+
+#define SUNXI_CPU_CFG			(SUNXI_TIMER_BASE + 0x13c)
+
+#ifndef __ASSEMBLY__
+void sunxi_board_init(void);
+void sunxi_reset(void);
+#endif /* __ASSEMBLY__ */
+
+#endif /* _CPU_H */
diff --git a/arch/arm/include/asm/arch-sunxi/spl.h b/arch/arm/include/asm/arch-sunxi/spl.h
new file mode 100644
index 0000000..ff871bc
--- /dev/null
+++ b/arch/arm/include/asm/arch-sunxi/spl.h
@@ -0,0 +1,20 @@
+/*
+ * This is a copy of omap3/spl.h:
+ *
+ * (C) Copyright 2012
+ * Texas Instruments, <www.ti.com>
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+#ifndef	_ASM_ARCH_SPL_H_
+#define	_ASM_SPL_H_
+
+#define BOOT_DEVICE_NONE	0
+#define BOOT_DEVICE_XIP		1
+#define BOOT_DEVICE_NAND	2
+#define BOOT_DEVICE_ONE_NAND	3
+#define BOOT_DEVICE_MMC2	5 /*emmc*/
+#define BOOT_DEVICE_MMC1	6
+#define BOOT_DEVICE_XIPWAIT	7
+#define BOOT_DEVICE_MMC2_2      0xff
+#endif
diff --git a/board/sunxi/Makefile b/board/sunxi/Makefile
new file mode 100644
index 0000000..559112e
--- /dev/null
+++ b/board/sunxi/Makefile
@@ -0,0 +1,11 @@
+#
+# (C) Copyright 2012 Henrik Nordstrom <henrik@henriknordstrom.net>
+#
+# Based on some other board Makefile
+#
+# (C) Copyright 2000-2003
+# Wolfgang Denk, DENX Software Engineering, wd@denx.de.
+#
+# SPDX-License-Identifier:	GPL-2.0+
+#
+obj-y	+= board.o
diff --git a/board/sunxi/board.c b/board/sunxi/board.c
new file mode 100644
index 0000000..328334a
--- /dev/null
+++ b/board/sunxi/board.c
@@ -0,0 +1,57 @@
+/*
+ * (C) Copyright 2012-2013 Henrik Nordstrom <henrik@henriknordstrom.net>
+ * (C) Copyright 2013 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
+ *
+ * (C) Copyright 2007-2011
+ * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
+ * Tom Cubie <tangliang@allwinnertech.com>
+ *
+ * Some board init for the Allwinner A10-evb board.
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#include <common.h>
+#include <asm/arch/clock.h>
+#include <asm/arch/dram.h>
+
+DECLARE_GLOBAL_DATA_PTR;
+
+/* add board specific code here */
+int board_init(void)
+{
+	int id_pfr1;
+
+	gd->bd->bi_boot_params = (PHYS_SDRAM_0 + 0x100);
+
+	asm volatile("mrc p15, 0, %0, c0, c1, 1" : "=r"(id_pfr1));
+	debug("id_pfr1: 0x%08x\n", id_pfr1);
+	/* Generic Timer Extension available? */
+	if ((id_pfr1 >> 16) & 0xf) {
+		debug("Setting CNTFRQ\n");
+		/* CNTFRQ == 24 MHz */
+		asm volatile("mcr p15, 0, %0, c14, c0, 0" : : "r"(24000000));
+	}
+
+	return 0;
+}
+
+int dram_init(void)
+{
+	gd->ram_size = get_ram_size((long *)PHYS_SDRAM_0, PHYS_SDRAM_0_SIZE);
+
+	return 0;
+}
+
+#ifdef CONFIG_SPL_BUILD
+void sunxi_board_init(void)
+{
+	unsigned long ramsize;
+
+	printf("DRAM:");
+	ramsize = sunxi_dram_init();
+	printf(" %lu MiB\n", ramsize >> 20);
+	if (!ramsize)
+		hang();
+}
+#endif
diff --git a/include/configs/sun7i.h b/include/configs/sun7i.h
new file mode 100644
index 0000000..9b693f7
--- /dev/null
+++ b/include/configs/sun7i.h
@@ -0,0 +1,24 @@
+/*
+ * (C) Copyright 2012-2013 Henrik Nordstrom <henrik@henriknordstrom.net>
+ * (C) Copyright 2013 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
+ *
+ * Configuration settings for the Allwinner A20 (sun7i) CPU
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+#ifndef __CONFIG_H
+#define __CONFIG_H
+
+/*
+ * A20 specific configuration
+ */
+#define CONFIG_SUN7I		/* sun7i SoC generation */
+
+#define CONFIG_SYS_PROMPT		"sun7i# "
+
+/*
+ * Include common sunxi configuration where most the settings are
+ */
+#include <configs/sunxi-common.h>
+
+#endif /* __CONFIG_H */
diff --git a/include/configs/sunxi-common.h b/include/configs/sunxi-common.h
new file mode 100644
index 0000000..3f7e314
--- /dev/null
+++ b/include/configs/sunxi-common.h
@@ -0,0 +1,141 @@
+/*
+ * (C) Copyright 2012-2012 Henrik Nordstrom <henrik@henriknordstrom.net>
+ *
+ * (C) Copyright 2007-2011
+ * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
+ * Tom Cubie <tangliang@allwinnertech.com>
+ *
+ * Configuration settings for the Allwinner sunxi series of boards.
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#ifndef _SUNXI_COMMON_CONFIG_H
+#define _SUNXI_COMMON_CONFIG_H
+
+/*
+ * High Level Configuration Options
+ */
+#define CONFIG_SUNXI		/* sunxi family */
+
+#include <asm/arch/cpu.h>	/* get chip and board defs */
+
+#define CONFIG_SYS_TEXT_BASE		0x4a000000
+
+/*
+ * Display CPU information
+ */
+#define CONFIG_DISPLAY_CPUINFO
+
+/* Serial & console */
+#define CONFIG_SYS_NS16550
+#define CONFIG_SYS_NS16550_SERIAL
+/* ns16550 reg in the low bits of cpu reg */
+#define CONFIG_SYS_NS16550_REG_SIZE	-4
+#define CONFIG_SYS_NS16550_CLK		24000000
+#define CONFIG_SYS_NS16550_COM1		SUNXI_UART0_BASE
+#define CONFIG_SYS_NS16550_COM2		SUNXI_UART1_BASE
+#define CONFIG_SYS_NS16550_COM3		SUNXI_UART2_BASE
+#define CONFIG_SYS_NS16550_COM4		SUNXI_UART3_BASE
+
+/* DRAM Base */
+#define CONFIG_SYS_SDRAM_BASE		0x40000000
+#define CONFIG_SYS_INIT_RAM_ADDR	0x0
+#define CONFIG_SYS_INIT_RAM_SIZE	0x8000	/* 32 KiB */
+
+#define CONFIG_SYS_INIT_SP_OFFSET \
+	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
+#define CONFIG_SYS_INIT_SP_ADDR \
+	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)
+
+#define CONFIG_NR_DRAM_BANKS		1
+#define PHYS_SDRAM_0			CONFIG_SYS_SDRAM_BASE
+#define PHYS_SDRAM_0_SIZE		0x80000000 /* 2 GiB */
+
+#define CONFIG_CMD_MEMORY
+#define CONFIG_CMD_SETEXPR
+
+#define CONFIG_SETUP_MEMORY_TAGS
+#define CONFIG_CMDLINE_TAG
+#define CONFIG_INITRD_TAG
+
+/* 4MB of malloc() pool */
+#define CONFIG_SYS_MALLOC_LEN		(CONFIG_ENV_SIZE + (4 << 20))
+
+/*
+ * Miscellaneous configurable options
+ */
+#define CONFIG_CMD_ECHO
+#define CONFIG_SYS_CBSIZE	256	/* Console I/O Buffer Size */
+#define CONFIG_SYS_PBSIZE	384	/* Print Buffer Size */
+#define CONFIG_SYS_MAXARGS	16	/* max number of command args */
+#define CONFIG_SYS_GENERIC_BOARD
+
+/* Boot Argument Buffer Size */
+#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE
+
+#define CONFIG_SYS_LOAD_ADDR		0x48000000 /* default load address */
+
+/* standalone support */
+#define CONFIG_STANDALONE_LOAD_ADDR	0x48000000
+
+#define CONFIG_SYS_HZ			1000
+
+/* baudrate */
+#define CONFIG_BAUDRATE			115200
+
+/* The stack sizes are set up in start.S using the settings below */
+#define CONFIG_STACKSIZE		(256 << 10)	/* 256 KiB */
+
+/* FLASH and environment organization */
+
+#define CONFIG_SYS_NO_FLASH
+
+#define CONFIG_SYS_MONITOR_LEN		(512 << 10)	/* 512 KiB */
+#define CONFIG_IDENT_STRING		" Allwinner Technology"
+
+#define CONFIG_ENV_SIZE			(128 << 10)	/* 128 KiB */
+
+#define CONFIG_EXTRA_ENV_SETTINGS \
+	"bootm_size=0x10000000\0"
+
+#define CONFIG_SYS_BOOT_GET_CMDLINE
+
+#include <config_cmd_default.h>
+
+#define CONFIG_FAT_WRITE	/* enable write access */
+
+#define CONFIG_SPL_FRAMEWORK
+#define CONFIG_SPL_LIBCOMMON_SUPPORT
+#define CONFIG_SPL_SERIAL_SUPPORT
+#define CONFIG_SPL_LIBGENERIC_SUPPORT
+
+#define CONFIG_SPL
+#define CONFIG_SPL_LDSCRIPT "arch/arm/cpu/armv7/sunxi/u-boot-spl-fel.lds"
+#define CONFIG_SPL_START_S_PATH "arch/arm/cpu/armv7/sunxi"
+#define CONFIG_SPL_TEXT_BASE		0x2000
+#define CONFIG_SPL_MAX_SIZE		0x4000		/* 16 KiB */
+/* end of 32 KiB in sram */
+#define LOW_LEVEL_SRAM_STACK		0x00008000 /* End of sram */
+#define CONFIG_SPL_STACK		LOW_LEVEL_SRAM_STACK
+#define CONFIG_SYS_SPL_MALLOC_START	0x4ff00000
+#define CONFIG_SYS_SPL_MALLOC_SIZE	0x00080000	/* 512 KiB */
+
+#undef CONFIG_CMD_FPGA
+#undef CONFIG_CMD_NET
+#undef CONFIG_CMD_NFS
+
+#define CONFIG_CONS_INDEX              1       /* UART0 */
+
+#if !defined CONFIG_ENV_IS_IN_MMC && \
+    !defined CONFIG_ENV_IS_IN_NAND && \
+    !defined CONFIG_ENV_IS_IN_FAT && \
+    !defined CONFIG_ENV_IS_IN_SPI_FLASH
+#define CONFIG_ENV_IS_NOWHERE
+#endif
+
+#ifndef CONFIG_SPL_BUILD
+#include <config_distro_defaults.h>
+#endif
+
+#endif /* _SUNXI_COMMON_CONFIG_H */
-- 
1.9.0

